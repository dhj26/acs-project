import argparse
from config import Config
from scenario_configs import SCENARIO_CONFIG

parser = argparse.ArgumentParser(
    prog='MARL State Reconstruction Experiments',
    description='Evaluates state reconstruction on MARL problems',
    epilog='0_0')

# Mode
parser.add_argument('-m', '--mode', help='select mode (s for sample, t for train sae, r for train rllib)')

# Generic (must be specified for all modes)
parser.add_argument('-c', '--scenario', default=None, help='VMAS scenario')

# Sampling
parser.add_argument('--sample_steps', default=200, type=int, help='number of sampling steps')
parser.add_argument('--render', action='store_true', default=False, help='stops rendering when sampling')
# Also: --num_vectorized_envs applies here too!

# Training SAE
parser.add_argument('-e', '--epochs', default=None, type=int, help='number of epochs of training')
parser.add_argument('--train_file', help='file to load for training data (sampled observations)')
parser.add_argument('--test_lim', default=256, type=int, help='limit on test set size')
parser.add_argument('--latent_dim', default=16, type=int, help='latent dimension of set autoencoder to use')
parser.add_argument('--batches_per_epoch', default=256, type=int, help='limit on batches to train with per epoch')

# Training policy
parser.add_argument('--sae_weights', default=None,
                    help=
                    'Trained set autoencoder weights file. If unspecified, trains randomly initialised SAE according '
                    'to --hidden_dim')
parser.add_argument('--hidden_dim', default=None, type=int,
                    help='Set autoencoder latent dimension if using untrained SAE.')
parser.add_argument('--use_recon_loss', action='store_true', default=False,
                    help='train the SAE online using the reconstruction loss')
parser.add_argument('--use_mlp_encoder', action='store_true', default=False,
                    help='use MLP encoder trained with RL losses instead of SAE')
parser.add_argument('--no_encoder', action='store_true', default=False,
                    help='train the policy network without an encoder')
parser.add_argument('--max_steps', default=200, type=int, help='max steps of policy')
parser.add_argument('--num_vectorized_envs', default=32, type=int)
parser.add_argument('--num_workers', default=5, type=int)
parser.add_argument('--num_cpus_per_worker', default=1, type=int)

# Misc
# Note: Use device cpu for sampling as VMAS seems to be slower on CUDA. Matteo kinda implied this too.
parser.add_argument('-d', '--device', default='cuda')

args = parser.parse_args()

# Set global configuration
Config.device = args.device

if args.mode == 's':

    import sample

    sample.sample_scenario(
        scenario_config=SCENARIO_CONFIG[args.scenario],
        n_steps=args.sample_steps,
        num_envs=args.num_vectorized_envs,
        render=args.render,
    )

elif args.mode == 't':

    import train

    train.train_sae(
        SCENARIO_CONFIG[args.scenario],
        args.train_file,
        args.epochs,
        args.test_lim,
        args.batches_per_epoch,
        args.latent_dim,
    )

elif args.mode == 'r':

    if not args.no_encoder:
        assert args.hidden_dim is not None

    import rllib_train

    rllib_train.train_policy(
        scenario_config=SCENARIO_CONFIG[args.scenario],
        sae_file=args.sae_weights,
        hidden_dim=args.hidden_dim,
        use_recon_loss=args.use_recon_loss,
        use_mlp_encoder=args.use_mlp_encoder,
        no_encoder=args.no_encoder,
        max_steps=args.max_steps,
        num_vectorized_envs=args.num_vectorized_envs,
        num_workers=args.num_workers,
        num_cpus_per_worker=args.num_cpus_per_worker
    )
