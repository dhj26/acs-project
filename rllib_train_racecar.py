from typing import Dict, Optional

import numpy as np
import ray
import wandb
from ray import tune
from ray.rllib import BaseEnv, Policy, RolloutWorker
from ray.rllib.agents.ppo import PPOTrainer
from ray.rllib.algorithms.callbacks import DefaultCallbacks, MultiCallbacks
from ray.rllib.evaluation import Episode, MultiAgentEpisode
from ray.rllib.utils.typing import PolicyID
from ray.rllib.models import ModelCatalog
from ray.tune import register_env
from ray.tune.integration.wandb import WandbLoggerCallback

from vmas import make_env, Wrapper

from policy_net import PolicyNet


class EvaluationCallbacks(DefaultCallbacks):
    def on_episode_step(
            self,
            *,
            worker: RolloutWorker,
            base_env: BaseEnv,
            episode: MultiAgentEpisode,
            **kwargs,
    ):
        info = episode.last_info_for()
        for a_key in info.keys():
            for b_key in info[a_key]:
                try:
                    episode.user_data[f"{a_key}/{b_key}"].append(info[a_key][b_key])
                except KeyError:
                    episode.user_data[f"{a_key}/{b_key}"] = [info[a_key][b_key]]

    def on_episode_end(
            self,
            *,
            worker: RolloutWorker,
            base_env: BaseEnv,
            policies: Dict[str, Policy],
            episode: MultiAgentEpisode,
            **kwargs,
    ):
        info = episode.last_info_for()
        for a_key in info.keys():
            for b_key in info[a_key]:
                metric = np.array(episode.user_data[f"{a_key}/{b_key}"])
                episode.custom_metrics[f"{a_key}/{b_key}"] = np.sum(metric).item()


def train_policy(scenario_config, sae_file, hidden_dim, use_recon_loss, max_steps, num_vectorized_envs, num_workers):
    from config import Config
    import torch

    # Register custom model which uses the autoencoder
    ModelCatalog.register_custom_model("policy_net", PolicyNet)

    scenario_name = scenario_config["scenario_name"]
    n_agents = scenario_config["n_agents"]
    obs_size = scenario_config["obs_size"]
    continuous_actions = scenario_config["continuous_actions"]

    # Initialise rllib and register the scenario
    if not ray.is_initialized():
        ray.init()
        print("Ray init!")

    from racecar_gym.envs import pettingzoo_api
    env_creator = lambda config: pettingzoo_api.env(scenario_path='MultiAgentAustria-v0')

    from ray.rllib.env import PettingZooEnv
    register_env('MultiAgentAustria-v0', lambda config: PettingZooEnv(env_creator(config)))

    if Config.device == 'cuda':
        gpu_count = 1
        num_gpus = 0.0001  # Driver GPU
        num_gpus_per_worker = (gpu_count - num_gpus - 0.2) / num_workers
    else:
        num_gpus = 0
        num_gpus_per_worker = 0

    print("rllib GPUs", num_gpus, "rllib GPUs/WORKER", num_gpus_per_worker)

    if sae_file is not None:
        sae = torch.load(
            sae_file,
            map_location=torch.device(Config.device)  # FIXME: temp fix until CUDA works
        )
        latent_dim = sae.encoder.hidden_dim
    else:
        from sae import sae_new as sae_model
        sae = sae_model.AutoEncoder(
            dim=scenario_config["obs_size"],
            hidden_dim=hidden_dim,
        ).to(Config.device)
        latent_dim = hidden_dim

    # Load data scaling variables
    mean = torch.load(f'scalers/mean_{scenario_name}.pt', map_location=torch.device(Config.device))
    std = torch.load(f'scalers/std_{scenario_name}.pt', map_location=torch.device(Config.device))

    pytorch_total_params = sum(p.numel() for p in sae.encoder.parameters() if p.requires_grad)
    print("SAE encoder params =", pytorch_total_params)

    tune.run(
        PPOTrainer,
        stop={"training_iteration": 5000},
        checkpoint_freq=1,
        keep_checkpoints_num=2,
        checkpoint_at_end=True,
        checkpoint_score_attr="episode_reward_mean",
        callbacks=[
            WandbLoggerCallback(
                project=f"acs_project",
                name="rllib_training",
                entity="dhjayalath",
                api_key="",
            )
        ],
        config={
            "seed": 0,
            "framework": "torch",
            "env": scenario_name,
            "kl_coeff": 0.01,
            "kl_target": 0.01,
            "lambda": 0.9,
            "clip_param": 0.2,
            "vf_loss_coeff": 1,
            "vf_clip_param": float("inf"),
            "entropy_coeff": 0,
            "train_batch_size": 60000,
            "rollout_fragment_length": 125,
            "sgd_minibatch_size": 4096,
            "model": {
                "custom_model": "policy_net",
                "custom_model_config": {
                    # "autoencoder": sae,
                    # "train_encoder": True if sae_file is None else False,  # Don't use RL losses to tune encoder
                    # "sae_latent_dim": latent_dim,
                    # "no_encoder": False,
                    # "n_agents": n_agents,
                    # "obs_size": obs_size,  # 6 for give_way, 16 for balance
                    "sae": sae,
                    "train_sae": True if (sae_file is None) and (use_recon_loss is False) else False,
                    "sae_encoded_dim": latent_dim,
                    "use_recon_loss": use_recon_loss,
                    "n_agents": n_agents,
                    "core_hidden_dim": 64,
                    "head_hidden_dim": 32,
                    "data_mean": mean,
                    "data_std": std,
                },
            },
            "num_sgd_iter": 40,
            "num_gpus": num_gpus,
            "num_workers": num_workers,
            "num_gpus_per_worker": num_gpus_per_worker,
            "num_envs_per_worker": num_vectorized_envs,
            "lr": 5e-5,
            "gamma": 0.99,
            "use_gae": True,
            "use_critic": True,
            "batch_mode": "truncate_episodes",
            "env_config": {
                "device": Config.device,
                "num_envs": num_vectorized_envs,
                "scenario_name": scenario_name,
                "max_steps": max_steps,
                # Scenario specific variables
                "scenario_config": {
                    "n_agents": n_agents,
                },
            },
            "evaluation_interval": 5,
            "evaluation_duration": 1,
            "evaluation_num_workers": 1,
            "evaluation_parallel_to_training": True,
            "evaluation_config": {
                "num_envs_per_worker": 1,
                "env_config": {
                    "num_envs": 1,
                },
                "callbacks": MultiCallbacks([EvaluationCallbacks]),
            },
            "callbacks": EvaluationCallbacks,
        },
    )


if __name__ == "__main__":
    train_policy()
