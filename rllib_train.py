from typing import Dict, Optional

import numpy as np
import ray
import wandb
from ray import tune
from ray.rllib import BaseEnv, Policy, RolloutWorker
from ray.rllib.agents.ppo import PPOTrainer
from ray.rllib.algorithms.callbacks import DefaultCallbacks, MultiCallbacks
from ray.rllib.evaluation import Episode, MultiAgentEpisode
from ray.rllib.utils.typing import PolicyID
from ray.rllib.models import ModelCatalog
from ray.tune import register_env
from ray.tune.integration.wandb import WandbLoggerCallback

from vmas import make_env, Wrapper

from policy_net import PolicyNet


def env_creator(config: Dict):
    env = make_env(
        scenario=config["scenario_name"],
        num_envs=config["num_envs"],
        device=config["device"],
        continuous_actions=config["continuous_actions"],
        wrapper=Wrapper.RLLIB,
        max_steps=config["max_steps"],
        # Scenario specific variables
        **config["scenario_config"],
    )
    return env
class EvaluationCallbacks(DefaultCallbacks):
    def on_episode_step(
            self,
            *,
            worker: RolloutWorker,
            base_env: BaseEnv,
            episode: MultiAgentEpisode,
            **kwargs,
    ):
        info = episode.last_info_for()
        for a_key in info.keys():
            for b_key in info[a_key]:
                try:
                    episode.user_data[f"{a_key}/{b_key}"].append(info[a_key][b_key])
                except KeyError:
                    episode.user_data[f"{a_key}/{b_key}"] = [info[a_key][b_key]]

    def on_episode_end(
            self,
            *,
            worker: RolloutWorker,
            base_env: BaseEnv,
            policies: Dict[str, Policy],
            episode: MultiAgentEpisode,
            **kwargs,
    ):
        info = episode.last_info_for()
        for a_key in info.keys():
            for b_key in info[a_key]:
                metric = np.array(episode.user_data[f"{a_key}/{b_key}"])
                episode.custom_metrics[f"{a_key}/{b_key}"] = np.sum(metric).item()


class RenderingCallbacks(DefaultCallbacks):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.frames = []

    def on_episode_step(
            self,
            *,
            worker: RolloutWorker,
            base_env: BaseEnv,
            policies: Optional[Dict[PolicyID, Policy]] = None,
            episode: Episode,
            **kwargs,
    ) -> None:
        self.frames.append(base_env.vector_env.try_render_at(mode="rgb_array"))

    def on_episode_end(
            self,
            *,
            worker: RolloutWorker,
            base_env: BaseEnv,
            policies: Dict[PolicyID, Policy],
            episode: Episode,
            **kwargs,
    ) -> None:
        vid = np.transpose(self.frames, (0, 3, 1, 2))
        episode.media["rendering"] = wandb.Video(
            vid, fps=1 / base_env.vector_env.env.world.dt, format="mp4"
        )
        self.frames = []


def make_smac_env(**smac_args):
    from smacv2_rllib_env import RLlibStarCraft2Env
    multi_env = RLlibStarCraft2Env(**smac_args)
    return multi_env


def train_policy(
        scenario_config,
        sae_file,
        hidden_dim,
        use_recon_loss,
        use_mlp_encoder,
        no_encoder,
        max_steps,
        num_vectorized_envs,
        num_workers,
        num_cpus_per_worker
):

    from config import Config
    import torch

    # Register custom model which uses the autoencoder
    ModelCatalog.register_custom_model("policy_net", PolicyNet)

    scenario_name = scenario_config["scenario_name"]
    n_agents = scenario_config["n_agents"]
    obs_size = scenario_config["obs_size"]
    continuous_actions = scenario_config["continuous_actions"]

    # Note: In rllib, Matteo recommends running VMAS on CPU as rllib has significant overheads for the GPU in sampling.
    # My own tests seem to agree with this. Should be set as cpu.
    vmas_device = "cpu"

    import os
    if sae_file is not None:
        sae_file = os.path.abspath(sae_file)
    cwd = os.getcwd()

    # Initialise rllib and register the scenario
    if not ray.is_initialized():
        ray.init()
        print("Ray init!")

    if "smac" not in scenario_name:
        register_env(scenario_name, lambda config: env_creator(config))

        if Config.device == 'cuda':
            num_gpus = 1  # Driver GPU
            num_gpus_per_worker = 0  # VMAS will be on CPU
        else:
            num_gpus = 0
            num_gpus_per_worker = 0

        print("VMAS Device", vmas_device, "rllib GPUs", num_gpus, "rllib GPUs/WORKER", num_gpus_per_worker)

    else:
        register_env("smacv2", lambda smac_args: make_smac_env(**smac_args))
        num_gpus = 0.0001
        num_gpus_per_worker = 0.1 #(num_gpus - 0.2) / num_workers

        print("rllib GPUs", num_gpus, "rllib GPUs/WORKER", num_gpus_per_worker)

    tune.run(
        PPOTrainer,
        stop={"training_iteration": 5000},
        checkpoint_freq=1,
        keep_checkpoints_num=2,
        checkpoint_at_end=True,
        checkpoint_score_attr="episode_reward_mean",
        callbacks=[
            WandbLoggerCallback(
                project=f"acs_project",
                name="rllib_training",
                entity="dhjayalath",
                api_key="",
            )
        ],
        config={
            "disable_env_checking": True,
            "seed": 0,
            "framework": "torch",
            "env": scenario_name,
            "kl_coeff": 0.01,
            "kl_target": 0.01,
            "lambda": 0.9,
            "clip_param": 0.2,
            "vf_loss_coeff": 1,
            "vf_clip_param": float("inf"),
            "entropy_coeff": 0,
            "train_batch_size": 20000,
            "rollout_fragment_length": 125,  # Should remain close to maximum time steps in episode (200) to avoid bias.
            "sgd_minibatch_size": 4096,
            "model": {
                "custom_model": "policy_net",
                "custom_model_config": {
                    "scenario_config": scenario_config,
                    "sae_file": sae_file,
                    "cwd": cwd,
                    "train_sae": True if (sae_file is None) and (use_recon_loss is False) else False,
                    "sae_encoded_dim": hidden_dim,
                    "use_recon_loss": use_recon_loss,
                    "use_mlp_encoder": use_mlp_encoder,
                    "no_encoder": no_encoder,
                    "n_agents": n_agents,
                    "core_hidden_dim": 64,
                    "head_hidden_dim": 32,
                },
            },
            "num_sgd_iter": 40,
            "num_gpus": num_gpus,
            "num_workers": num_workers,
            "num_gpus_per_worker": num_gpus_per_worker,
            "num_cpus_per_worker": num_cpus_per_worker,
            "num_envs_per_worker": num_vectorized_envs,
            "lr": 5e-5,
            "gamma": 0.99,
            "use_gae": True,
            "use_critic": True,
            "batch_mode": "truncate_episodes",
            "env_config": {
                "device": vmas_device,
                "num_envs": num_vectorized_envs,
                "scenario_name": scenario_name,
                "continuous_actions": continuous_actions,
                "max_steps": max_steps,
                # Scenario specific variables
                "scenario_config": {
                    "n_agents": n_agents,
                },
            } if "smac" not in scenario_name else
            {
                "capability_config": scenario_config["distribution_config"],
                "map_name": "10gen_terran",
                "debug": False,
                "conic_fov": False,
                "obs_own_pos": True,
                "use_unit_ranges": True,
                "min_attack_range": 2,
            },
            "evaluation_interval": 5,
            "evaluation_duration": 1,
            "evaluation_num_workers": 1,
            "evaluation_parallel_to_training": True,
            "evaluation_config": {
                "num_envs_per_worker": 1,
                "env_config": {
                    "num_envs": 1,
                } if "smac" not in scenario_name else
                {
                },
                "callbacks": MultiCallbacks([RenderingCallbacks, EvaluationCallbacks]),
            },
            "callbacks": EvaluationCallbacks,
        },
    )


if __name__ == "__main__":
    train_policy()
