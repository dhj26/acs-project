from typing import Dict, Union, List

import torch
from torch import nn, TensorType
from contextlib import nullcontext
from ray.rllib.models.torch.torch_modelv2 import TorchModelV2
from ray import rllib
import numpy


# Custom model which implements a simple neural network as the policy network.
# Should use the trained autoencoder to encode the latent observations that are received.

class SAEModel(TorchModelV2, nn.Module):

    def __init__(self, observation_space, action_space, num_outputs, model_config, name):
        TorchModelV2.__init__(self, observation_space, action_space, num_outputs, model_config, name)
        nn.Module.__init__(self)

        self.train_encoder = model_config["custom_model_config"]["train_encoder"]
        num_observations = int(numpy.product(observation_space.shape))
        sae_latent_dim = model_config["custom_model_config"]["sae_latent_dim"]
        self.no_encoder = model_config["custom_model_config"]["no_encoder"]
        self.n_agents = model_config["custom_model_config"]["n_agents"]
        self.obs_size = model_config["custom_model_config"]["obs_size"]
        self.num_outputs = num_outputs

        if self.no_encoder is True:
            self.mlp_in = nn.Linear(in_features=self.n_agents * self.obs_size, out_features=128)
            self.mlp_out = nn.Linear(in_features=128, out_features=sae_latent_dim)
        else:
            self.sae = model_config["custom_model_config"]["autoencoder"]

        self.core_input_layer = nn.Linear(in_features=sae_latent_dim, out_features=64)
        self.core_output_layer = nn.Linear(in_features=64, out_features=64)

        # Construct policy network heads
        self.policy_heads = []
        for i in range(self.n_agents):
            seq = nn.Sequential(
                nn.Linear(in_features=64 + self.obs_size, out_features=64), # Skip connection
                nn.Tanh()
            )
            lin_out = nn.Linear(in_features=64, out_features=action_space[i].n)
            nn.init.normal_(lin_out.weight, mean=0.0, std=0.03)
            nn.init.normal_(lin_out.bias, mean=0.0, std=0.03)
            seq.add_module('lin_out', lin_out)
            self.policy_heads.append(seq)
        self.policy_heads = nn.ModuleList(self.policy_heads)

        self.value_input_layer = nn.Linear(in_features=64, out_features=64)
        self.value_output_layer = nn.Linear(in_features=64, out_features=1)

        self.inter_act = nn.Tanh()
        self.final_act = nn.Identity()

        self.current_value = None

    def forward(self, inputs, state, seq_lens):

        observation = inputs["obs_flat"].float()  # [batches, agents * obs_size]
        n_batches = observation.shape[0]

        if self.no_encoder is True:

            # If no SAE is used, then pass observation through an MLP of similar size
            observation = self.inter_act(self.mlp_in(observation))
            observation = self.inter_act(self.mlp_out(observation))

        else:

            # Format observations into batch
            obs_struct = observation.reshape(-1, self.n_agents, self.obs_size)  # [batches, agents, obs_size]
            observation = torch.flatten(obs_struct, start_dim=0, end_dim=1)  # [batches * agents, obs_size]
            batch = torch.div(
                torch.tensor(list(range(n_batches * self.n_agents)), device='cuda'),
                self.n_agents, rounding_mode="trunc"
            )

            # Pass through encoder with/without frozen weights
            with nullcontext() if self.train_encoder else torch.no_grad():
                enc_obs = self.sae.encoder(observation, batch=batch, n_batches=n_batches)
                observation = enc_obs

        # Core network
        x = self.inter_act(self.core_input_layer(observation))
        x = self.inter_act(self.core_output_layer(x))

        # Policy heads
        p_components = []
        for i, policy_head in enumerate(self.policy_heads):
            p_components.append(policy_head(torch.cat((x, obs_struct[:, i]), dim=1)))
        p = torch.cat(p_components, dim=1)

        # Value head
        v = self.inter_act(self.value_input_layer(x))
        v = self.final_act(self.value_output_layer(v))

        self.current_value = v.squeeze(1)
        logits = p

        return logits, state

    def value_function(self):
        return self.current_value

    def custom_loss(
        self, policy_loss: TensorType, loss_inputs: Dict[str, TensorType]
    ) -> Union[List[TensorType], TensorType]:

        # observation = loss_inputs["obs_flat"].float()  # [batches, agents * obs_size]
        # n_batches = observation.shape[0]
        # obs_struct = observation.reshape(-1, self.n_agents, self.obs_size)  # [batches, agents, obs_size]
        # observation = torch.flatten(obs_struct, start_dim=0, end_dim=1)  # [batches * agents, obs_size]
        # batch = torch.div(
        #     torch.tensor(list(range(n_batches * self.n_agents)), device='cuda'),
        #     self.n_agents, rounding_mode="trunc"
        # )

        # _, _ = self.sae(observation, batch=batch)

        train_loss_vars = self.sae.loss()
        sae_loss = train_loss_vars["loss"]
        print(sae_loss)


        return policy_loss + sae_loss