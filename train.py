from sae import sae_new as sae_model
from torch.optim import Adam
from config import Config
import torch
import time


def _load_data(data_file, scenario_name, time_str):
    print(f"Loading {data_file}...")

    # Load file containing all the observations to encode. We expect that
    # the file contains a tensor of shape (steps, agents, envs, obs shape)
    data = torch.load(data_file).to(Config.device).permute(0, 2, 1, 3)

    # We want: (samples, agents, obs shape) so that we can learn to encode
    # all four observations from the agents in one time step.
    data = torch.flatten(data, start_dim=0, end_dim=1)

    # Shuffle the data (but only in the first dimension)
    data = data[torch.randperm(data.size()[0])]

    # Cache mean and standard deviation for rescaling later
    mean = data.mean(0)
    std = data.std(0)
    torch.save(mean, f'scalers/mean_{scenario_name}_{time_str}.pt')
    torch.save(std, f'scalers/std_{scenario_name}_{time_str}.pt')

    # Normalise observations to zero mean and unit variance in feature channels
    data = (data - mean) / std

    # Replace any NaNs introduced by zero-division
    data[data != data] = 0

    print("Loaded data with shape", data.shape)

    return data


def _train_test_split(data, train_proportion, test_lim):
    n_train_samples = int(len(data) * train_proportion)
    n_test_samples = min(len(data) - n_train_samples, test_lim)
    n_train_samples = len(data) - n_test_samples

    return data[:n_train_samples], data[n_train_samples: n_train_samples + n_test_samples]


def train_sae(scenario_config, data_file, n_epochs, test_lim, batches_per_epoch, hidden_dim):

    time_str = time.strftime("%Y%m%d-%H%M%S")

    scenario_name = scenario_config["scenario_name"]
    set_size = scenario_config["n_agents"]

    # Load and process data
    data = _load_data(data_file, scenario_name, time_str)
    train, test = _train_test_split(data, train_proportion=0.8, test_lim=test_lim)

    # Flatten first two dimensions to put samples and agents together to get [samples, obs_dim]
    # as agents will be accounted for by the batch index
    train = torch.flatten(train, start_dim=0, end_dim=1).to(Config.device)
    test = torch.flatten(test, start_dim=0, end_dim=1).to(Config.device)

    batch_train = torch.div(torch.tensor(list(range(batches_per_epoch))), set_size, rounding_mode="trunc").to(Config.device)
    batch_test = torch.div(torch.tensor(list(range(test.shape[0]))), set_size, rounding_mode="trunc").to(Config.device)

    # Construct the autoencoder
    model_dim = data.shape[-1]
    sae = sae_model.AutoEncoder(dim=model_dim, hidden_dim=hidden_dim).to(Config.device)
    optimizer = Adam(sae.parameters())

    n_epochs = len(train) // batches_per_epoch if n_epochs is None else min(len(train) // batches_per_epoch, n_epochs)

    print(f"Training model using device {Config.device} for {n_epochs} epochs")

    import wandb
    run = wandb.init(
        project="acs-project",
        entity="dhjayalath",
        name="train_sae",
        sync_tensorboard=True,
        config={
            "epochs": n_epochs,
            "train_size": len(train),
            "test_size": len(test),
        }
    )

    for epoch in range(n_epochs):

        optimizer.zero_grad()

        x = train[epoch * batches_per_epoch: (epoch + 1) * batches_per_epoch]
        xr, _ = sae(x, batch=batch_train)
        train_loss_vars = sae.loss()
        sae_loss = train_loss_vars["loss"]
        sae_loss.backward()

        optimizer.step()

        with torch.no_grad():

            xr, _ = sae(test, batch=batch_test)
            test_loss_vars = sae.loss()

            if epoch % 1000 == 0:
                print("\t Epoch", epoch)
                print("----- TRAIN -----")
                print(train_loss_vars)
                print("----- TEST -----")
                print(test_loss_vars)

                if len(xr) >= set_size:
                    print("Length (source, recon)", test[0:set_size].shape, xr[0:set_size].shape)
                    print("Source", test[0:set_size])
                    print("Recon", xr[0:set_size])

                if epoch % 5000 == 0 and epoch != 0:
                    time_str = time.strftime("%Y%m%d-%H%M%S")
                    file_str = f"weights/sae_{scenario_name}_{epoch}_{time_str}.pt"
                    torch.save(sae, file_str)

            wandb.log({
                "train_loss": train_loss_vars["loss"],
                "mse_loss": train_loss_vars["mse_loss"],
                "size_loss": train_loss_vars["size_loss"],
                "corr": train_loss_vars["corr"],
                "test_loss": test_loss_vars["loss"],
                "test_mse_loss": test_loss_vars["mse_loss"],
                "test_size_loss": test_loss_vars["size_loss"],
                "test_corr": test_loss_vars["corr"],
            })

    run.finish()

    # Show an example reconstruction
    print("Showing reconstruction on random sample...")
    with torch.no_grad():

        xr, _ = sae(test, batch=batch_test)

        print("Length (source, recon)", test[0].shape, xr[0].shape)
        print("Source", test[0:set_size])
        print("Recon", xr[0:set_size])

    # Save model
    file_str = f"weights/sae_{scenario_name}_{time_str}.pt"
    torch.save(sae, file_str)
    print(f"Saved model to {file_str}")
