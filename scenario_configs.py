SCENARIO_CONFIG = {
    # "waterfall": {
    #     "scenario_name": "waterfall",
    #     "n_agents": 4,
    #     "obs_size"
    # }
    "balance": {
        "scenario_name": "balance",
        "n_agents": 4,
        "obs_size": 16,
        "n_actions": 4,
        "continuous_actions": False,
        "reset_after": None,
    },
    "give_way": {
        "scenario_name": "give_way",
        "n_agents": 2,
        "obs_size": 6,
        "n_actions": 4,
        "continuous_actions": False,
        "reset_after": None,
    },
    "flocking": {
        "scenario_name": "flocking",
        "n_agents": 4,
        "obs_size": 18,
        "n_actions": 4,
        "continuous_actions": False,
        "reset_after": 100,
    },
    "custom_dispersion": {
        "scenario_name": "custom_dispersion",
        "n_agents": 4,
        "obs_size": 48,
        "n_actions": 4,
        "continuous_actions": False,
        "reset_after": 100
    },
    "custom_dispersion_sparse": {
        "scenario_name": "custom_dispersion_sparse",
        "n_agents": 4,
        "obs_size": 48,
        "n_actions": 4,
        "continuous_actions": False,
        "reset_after": 100
    },
    "discovery": {
        "scenario_name": "discovery",
        "n_agents": 4,
        "obs_size": 21,
        "n_actions": 4,
        "continuous_actions": False,
        "reset_after": 200,
    },
    "custom_discovery": {
        "scenario_name": "custom_discovery",
        "n_agents": 4,
        "obs_size": 106,
        "n_actions": 4,
        "continuous_actions": False,
        "reset_after": 200,
    },
    "smacv2": {
        "scenario_name": "smacv2",
        "n_agents": 5,
        "obs_size": 82,
        "n_actions": 11,
        "continuous_actions": False,
        "reset_after": 200,
        "distribution_config": {
            "n_units": 5,
            "n_agents": 5,
            "n_enemies": 5,
            "team_gen": {
                "n_agents": 5,
                "n_units": 5,
                "n_enemies": 5,
                "dist_type": "weighted_teams",
                "unit_types": ["marine"],
                "weights": [1.0],
                "observe": True,
            },
            "start_positions": {
                "dist_type": "surrounded_and_reflect",
                "p": 0.5,
                "n_enemies": 5,
                "n_agents": 5,
                "n_units": 5,
                "map_x": 32,
                "map_y": 32,
            },
        }
    }

}
