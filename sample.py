from vmas.simulator.utils import save_video
from vmas import make_env
from config import Config
import torch
import time
import random
import numpy as np

random.seed(42)


def _generate_random_action(previous_act, n_actions, num_envs, drift=0.8):

    eps = torch.rand((num_envs, 1))
    rand_action = torch.randint(low=0, high=n_actions + 1, size=(num_envs, 1))

    if previous_act is None:
        return rand_action
    else:
        new_act = previous_act.clone()
        modify = eps > drift
        new_act[modify] = rand_action[modify]
        return new_act

# Sample observations from an environment and save them to file
def sample_scenario(
        scenario_config,
        n_steps,
        num_envs,
        render
):
    init_time = time.time()

    scenario_name = scenario_config["scenario_name"]
    n_agents = scenario_config["n_agents"]
    n_actions = scenario_config["n_actions"]
    continuous_actions = scenario_config["continuous_actions"]

    print(f"Making environment with device {Config.device}...")

    if "smac" not in scenario_name:
        env = make_env(
            scenario=scenario_name,
            num_envs=num_envs,
            device=Config.device,
            continuous_actions=continuous_actions,
            n_agents=n_agents,
        )
        obs_size = env.observation_space[0].shape[0]
    else:
        num_envs = 1
        from smacv2.env.starcraft2.wrapper import StarCraftCapabilityEnvWrapper
        env = StarCraftCapabilityEnvWrapper(
            capability_config=scenario_config["distribution_config"],
            map_name="10gen_terran",
            debug=False,
            conic_fov=False,
            obs_own_pos=True,
            use_unit_ranges=True,
            min_attack_range=2,
        )
        print("Loaded SMAC environment with info:")
        print(env.get_env_info())
        obs_size = env.get_env_info()["obs_shape"]
        env.reset()
        obs = env.get_obs()
        state = env.get_state()

    agent_observations = torch.empty((n_steps, n_agents, num_envs, obs_size))

    print("Sampling observations from environment...")
    prev_act = [None for _ in range(n_agents)]
    for s in range(n_steps):
        actions = []

        for i in range(n_agents):
            if "smac" not in scenario_name:
                act = _generate_random_action(prev_act[i], n_actions, num_envs)
                actions.append(act)
                prev_act[i] = act

            else:
                avail_actions = env.get_avail_agent_actions(i)
                avail_actions_ind = np.nonzero(avail_actions)[0]
                act = np.random.choice(avail_actions_ind)
                actions.append(act)

        if "smac" not in scenario_name:

            obs, rews, dones, info = env.step(actions)
            agent_observations[s] = torch.stack(obs)

            # Reset environments that are done so that we don't sample
            # out of distribution
            for i, done in enumerate(dones):
                if done.item() is True:
                    env.reset_at(i)
        else:
            reward, terminated, _ = env.step(actions)

            obs = env.get_obs()
            state = env.get_state()

            agent_observations[s] = torch.tensor(obs).unsqueeze(1)

            if terminated is True:
                env.reset()

        if scenario_config["reset_after"] is not None:
            if s % scenario_config["reset_after"] == 0:
                env.reset()

        if render:
            if "smac" not in scenario_name:
                env.render(
                    mode="rgb_array",
                    agent_index_focus=None,
                    visualize_when_rgb=True,
                )
            else:
                env.render()

        if s % 10 == 0:
            print(f"{s}/{n_steps}")

    timestr = time.strftime("%Y%m%d-%H%M%S")
    torch.save(agent_observations, f'samples/{scenario_name}_{timestr}.pt')
    print(f"Saved {agent_observations.shape} observations as {scenario_name}_{timestr}.pt")

    total_time = time.time() - init_time
    print(
        f"It took: {total_time}s for {n_steps} steps of {num_envs} parallel environments on device {Config.device}"
    )
