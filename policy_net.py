from contextlib import nullcontext

from ray.rllib.models.torch.torch_modelv2 import TorchModelV2
import torch
from torch.autograd import Variable


class PolicyNet(TorchModelV2, torch.nn.Module):

    def __init__(self, observation_space, action_space, num_outputs, model_config, name, *args, **kwargs):

        # Call super class constructors
        TorchModelV2.__init__(self, observation_space, action_space, num_outputs, model_config, name)
        torch.nn.Module.__init__(self)

        # Process keyword arguments
        sae_file = kwargs.get("sae_file")
        self.scenario_config = kwargs.get("scenario_config")
        scenario_name = self.scenario_config["scenario_name"]
        self.train_sae = kwargs.get("train_sae")
        self.sae_encoded_dim = kwargs.get("sae_encoded_dim")
        self.n_agents = kwargs.get("n_agents")
        self.core_hidden_dim = kwargs.get("core_hidden_dim")
        self.head_hidden_dim = kwargs.get("head_hidden_dim")
        self.use_recon_loss = kwargs.get("use_recon_loss")
        self.use_mlp_encoder = kwargs.get("use_mlp_encoder")
        self.no_encoder = kwargs.get("no_encoder")
        cwd = kwargs.get("cwd")

        if self.use_mlp_encoder:
            self.mlp_encoder = torch.nn.Sequential(
                torch.nn.Linear(
                    in_features=self.scenario_config["obs_size"] * self.scenario_config["n_agents"],
                    out_features=128,
                ),
                torch.nn.Tanh(),
                torch.nn.Linear(
                    in_features=128,
                    out_features=self.sae_encoded_dim,
                ),
            )
        else:
            if self.no_encoder is False:

                if sae_file is not None:
                    self.sae = torch.load(
                        sae_file,
                        map_location=torch.device("cuda" if torch.cuda.is_available() else "cpu")
                    )
                    assert self.sae.encoder.hidden_dim == self.sae_encoded_dim
                else:
                    from sae import sae_new as sae_model
                    self.sae = sae_model.AutoEncoder(
                        dim=self.scenario_config["obs_size"],
                        hidden_dim=self.sae_encoded_dim,
                    ).to("cuda" if torch.cuda.is_available() else "cpu")

                # Freeze the encoder if we don't want to train it
                if not self.train_sae:
                    # # FIXME: This will break training with reconstruction losses
                    # assert self.use_recon_loss is False  # We should not be training with any losses if freezing
                    for p in self.sae.parameters():
                        p.requires_grad = False

                pytorch_trainable_params = sum(p.numel() for p in self.sae.encoder.parameters() if p.requires_grad)
                pytorch_params = sum(p.numel() for p in self.sae.encoder.parameters())
                print("SAE params =", pytorch_params, "SAE trainable params =", pytorch_trainable_params)

        # Load data scaling variables
        self.data_mean = torch.load(f'{cwd}/scalers/mean_{scenario_name}.pt',
                                    map_location=torch.device("cuda" if torch.cuda.is_available() else "cpu"))
        self.data_std = torch.load(f'{cwd}/scalers/std_{scenario_name}.pt',
                                   map_location=torch.device("cuda" if torch.cuda.is_available() else "cpu"))

        # SAE optimiser
        if self.use_recon_loss:
            assert self.train_sae is False  # SAE should be frozen w.r.t policy losses
            self.sae_opt = torch.optim.Adam(self.sae.parameters())

        # Core policy network
        self.core_network = torch.nn.Sequential(
            torch.nn.Linear(
                in_features=self.sae_encoded_dim if not self.no_encoder else self.scenario_config["obs_size"] * self.scenario_config["n_agents"],
                out_features=self.core_hidden_dim
            ),
            torch.nn.Tanh(),
            torch.nn.Linear(
                in_features=self.core_hidden_dim,
                out_features=self.core_hidden_dim
            ),
            torch.nn.Tanh()
        )

        # Policy net heads
        self.policy_heads = []
        for i in range(self.n_agents):
            policy_head = torch.nn.Sequential(
                torch.nn.Linear(
                    in_features=self.core_hidden_dim + self.scenario_config["obs_size"],  # For concatenating agent obs
                    out_features=self.head_hidden_dim
                ),
                torch.nn.Tanh()
            )

            # Initialise final layer with zero mean and very small variance
            lin_out = torch.nn.Linear(
                in_features=self.head_hidden_dim,
                out_features=action_space[i].n if "smac" not in scenario_name else self.scenario_config["n_actions"]
            )
            torch.nn.init.normal_(lin_out.weight, mean=0.0, std=0.03)
            torch.nn.init.normal_(lin_out.bias, mean=0.0, std=0.03)

            policy_head.add_module('lin_out', lin_out)
            self.policy_heads.append(policy_head)
        self.policy_heads = torch.nn.ModuleList(self.policy_heads)

        # Value net head
        # TODO: Should this have multiple outputs?
        self.value_head = torch.nn.Sequential(
            torch.nn.Linear(in_features=self.core_hidden_dim, out_features=self.head_hidden_dim),
            torch.nn.Tanh(),
            torch.nn.Linear(in_features=self.head_hidden_dim, out_features=1)
        )
        self.current_value = None

    def forward(self, inputs, state, seq_lens):

        observation = inputs["obs_flat"] if "smac" not in self.scenario_config["scenario_name"] else inputs["obs"]["obs"]  # [batches, agents * obs_size]
        n_batches = observation.shape[0]

        observation = observation.reshape(n_batches, self.n_agents, -1)  # [batches, agents, obs_size]
        agent_features = observation.clone()

        # Rescale observations
        observation = (observation - self.data_mean) / self.data_std
        observation[observation != observation] = 0  # Replace NaNs introduced by zero-division with zero

        observation = torch.flatten(observation, start_dim=0, end_dim=1)  # [batches * agents, obs_size]

        batch = torch.arange(n_batches, device=observation.device).repeat_interleave(self.n_agents)

        # (Optionally) optimise set autoencoder with reconstruction losses
        if self.use_recon_loss is True:
            for p in self.sae.parameters():
                p.requires_grad = True
            self.sae_opt.zero_grad()
            xr, _ = self.sae(observation, batch=batch)
            recon_loss = self.sae.loss()["loss"]  # Combination of MSE and size loss
            recon_loss = Variable(recon_loss, requires_grad=True)
            recon_loss.backward()
            self.sae_opt.step()
            for p in self.sae.parameters():
                p.requires_grad = False

        x = observation

        # Apply encoder
        if not self.no_encoder:
            if self.use_mlp_encoder:
                x = x.reshape(n_batches, -1)
                x = self.mlp_encoder(x)
            else:
                x = self.sae.encoder(x, batch, n_batches=n_batches)
        else:
            x = x.reshape(n_batches, -1)

        x = self.core_network(x)

        v = self.value_head(x.clone())
        self.current_value = v.squeeze(1)

        actions_logits = []
        for i, policy_head in enumerate(self.policy_heads):
            p = policy_head(
                torch.cat(
                    (
                        x.clone(),  # [batches, latent]
                        agent_features[:, i].clone(),  # [batches, obs_size]
                    ),
                    dim=1,
                )  # [batches, latent + obs_size]
            )  # [batches, actions]
            actions_logits.append(p)
        logits = torch.cat(actions_logits, dim=1)  # [batches, actions * n_agents]

        return logits, state

    def value_function(self):
        return self.current_value
