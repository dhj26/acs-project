#  Copyright (c) 2022.
#  ProrokLab (https://www.proroklab.org/)
#  All rights reserved.
from typing import Callable

import torch

from vmas import render_interactively
from vmas.simulator.core import Agent, Landmark, Sphere, World, Entity
from vmas.simulator.scenario import BaseScenario
from vmas.simulator.sensors import Lidar
from vmas.simulator.utils import Color


class Scenario(BaseScenario):
    def make_world(self, batch_dim: int, device: torch.device, **kwargs):
        n_agents = kwargs.get("n_agents", 4)
        self.share_reward = kwargs.get("share_reward", False)
        self.penalise_by_time = kwargs.get("penalise_by_time", False)

        n_food = n_agents

        # Make world
        world = World(batch_dim, device)
        # Add agents
        food_entity_filter: Callable[[Entity], bool] = lambda e: "food" not in e.name  # Don't detect food with lidar
        for i in range(n_agents):
            # Constraint: all agents have same action range and multiplier
            agent = Agent(
                name=f"agent {i}",
                collide=True,  # Agents should be able to collide
                shape=Sphere(radius=0.1),
                sensors=[
                    Lidar(
                        world,
                        n_rays=32,
                        max_range=0.4,
                        entity_filter=food_entity_filter,
                    )
                ],
            )
            world.add_agent(agent)
        # Add landmarks
        for i in range(n_food):
            food = Landmark(
                name=f"food {i}",
                collide=False,
                shape=Sphere(radius=0.02),
                color=Color.GREEN,
            )
            world.add_landmark(food)

        return world

    def reset_world_at(self, env_index: int = None):
        for agent in self.world.agents:
            if env_index is None:
                agent.collided = torch.full(
                    (self.world.batch_dim,), False, device=self.world.device
                )
            else:
                agent.collided[env_index] = False

            agent.set_pos(
                # TODO: Reset agents such that they are not colliding and randomly spawned
                torch.zeros(
                    (1, self.world.dim_p) if env_index is not None else (self.world.batch_dim, self.world.dim_p),
                    device=self.world.device,
                    dtype=torch.float32
                ).uniform_(-1.0, 1.0),
                batch_index=env_index,
            )
        for landmark in self.world.landmarks:
            landmark.set_pos(
                torch.zeros(
                    (1, self.world.dim_p)
                    if env_index is not None
                    else (self.world.batch_dim, self.world.dim_p),
                    device=self.world.device,
                    dtype=torch.float32,
                ).uniform_(
                    -1.0,
                    1.0,
                ),
                batch_index=env_index,
            )
            if env_index is None:
                landmark.eaten = torch.full(
                    (self.world.batch_dim,), False, device=self.world.device
                )
                landmark.just_eaten = torch.full(
                    (self.world.batch_dim,), False, device=self.world.device
                )
                landmark.reset_render()
            else:
                landmark.eaten[env_index] = False
                landmark.just_eaten[env_index] = False
                landmark.is_rendering[env_index] = True

    def reward(self, agent: Agent):

        is_first = agent == self.world.agents[0]
        is_last = agent == self.world.agents[-1]

        rews = torch.zeros(self.world.batch_dim, device=self.world.device)

        for landmark in self.world.landmarks:

            # Compute only once by doing it on first agent
            if is_first:

                # Compute how many of the agents are on this particular food
                landmark.how_many_on_food = torch.stack(
                    [
                        torch.linalg.vector_norm(
                            a.state.pos - landmark.state.pos, dim=1
                        )
                        < a.shape.radius + landmark.shape.radius
                        for a in self.world.agents
                    ],
                    dim=1,
                ).sum(-1)
                landmark.anyone_on_food = landmark.how_many_on_food > 0
                landmark.just_eaten[landmark.anyone_on_food] = True

            assert (landmark.how_many_on_food <= len(self.world.agents)).all()

            if is_last:
                landmark.eaten += landmark.just_eaten
                landmark.just_eaten[:] = False
                landmark.is_rendering[landmark.eaten] = False

        # Penalise agents for colliding
        for a in self.world.agents:
            if a != agent:
                overlap = self.world.is_overlapping(a, agent)
                agent.collided = torch.logical_or(agent.collided, overlap)

        if is_first:

            # Determine if landmarks are all eaten
            landmarks_eaten = torch.all(
                torch.stack(
                    [landmark.eaten for landmark in self.world.landmarks],
                    dim=1,
                ),
                dim=-1,
            )
            rew = torch.zeros_like(landmarks_eaten)
            rew[landmarks_eaten] = 1.0

            # Determine if collision occurred
            collisions = torch.any(
                torch.stack(
                    [agent.collided for agent in self.world.agents],
                    dim=1,
                ),
                dim=1,
            )
            rew[collisions] = -1.0

            # Let's have a *global* sparse reward
            self.sparse_rew = rew

            # Determine if we are done
            self._done = torch.logical_or(collisions, landmarks_eaten)
            # Hopefully, done() is called after we loop through all the agents

        return self.sparse_rew

    def observation(self, agent: Agent):
        obs = []
        for landmark in self.world.landmarks:
            obs.append(
                torch.cat(
                    [
                        landmark.state.pos - agent.state.pos,
                        landmark.eaten.to(torch.int).unsqueeze(-1),
                    ],
                    dim=-1,
                )
            )

        final_obs = torch.cat(
            [
                agent.state.pos,
                agent.state.vel,
                *obs,
                agent.sensors[0].measure(),
            ],
            dim=-1,
        )
        return final_obs
    def done(self):
        return self._done

if __name__ == "__main__":
    render_interactively(
        __file__,
        control_two_agents=True,
        n_agents=4,
        share_reward=True,
        penalise_by_time=False,
    )
